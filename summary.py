import numpy as np
import tensorflow as tf
import random
import matplotlib.pyplot as plt

from poke_env.player.env_player import Gen8EnvSinglePlayer
from poke_env.player.random_player import RandomPlayer
from poke_env.player.player import Player

from rl.agents.dqn import DQNAgent
from rl.policy import LinearAnnealedPolicy, EpsGreedyQPolicy
from rl.memory import SequentialMemory
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import load_model

model = load_model("model_10")
model2 = load_model("model_9")
model3 = load_model("model_8")

print(model.summary())
print(model2.summary())
print(model3.summary())
