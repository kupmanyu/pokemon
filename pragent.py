# -*- coding: utf-8 -*-
import numpy as np
import tensorflow as tf
import random
import matplotlib.pyplot as plt

from poke_env.player.env_player import Gen8EnvSinglePlayer
from poke_env.player.random_player import RandomPlayer
from poke_env.player.player import Player

from rl.agents.dqn import DQNAgent
from rl.policy import LinearAnnealedPolicy, EpsGreedyQPolicy
from rl.memory import SequentialMemory
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import load_model
from baselines import deepq

# We define our RL player
# It needs a state embedder and a reward computer, hence these two methods
class SimpleRLPlayer(Gen8EnvSinglePlayer):
    def embed_battle(self, battle):
        # -1 indicates that the move does not have a base power
        # or is not available
        moves_base_power = -np.ones(4)
        moves_dmg_multiplier = np.ones(4)
        for i, move in enumerate(battle.available_moves):
            moves_base_power[i] = (
                move.base_power / 100
            )  # Simple rescaling to facilitate learning
            if move.type:
                moves_dmg_multiplier[i] = move.type.damage_multiplier(
                    battle.opponent_active_pokemon.type_1,
                    battle.opponent_active_pokemon.type_2,
                )

        # We count how many pokemons have not fainted in each team
        remaining_mon_team = (
            len([mon for mon in battle.team.values() if mon.fainted]) / 6
        )
        remaining_mon_opponent = (
            len([mon for mon in battle.opponent_team.values() if mon.fainted]) / 6
        )

        # Final vector with 10 components
        return np.concatenate(
            [
                moves_base_power,
                moves_dmg_multiplier,
                [remaining_mon_team, remaining_mon_opponent],
            ]
        )

    def compute_reward(self, battle) -> float:
        reward = self.reward_computing_helper(
            battle, fainted_value=2, hp_value=1, victory_value=30
        )
        return reward

class TrainedRLPlayer(Player):
      def choose_move(self, battle):
        if (decision(0.7)):
            #try:
            #model2.load_weights(cfilepath)
            #except:
            #    model2.load_weights(old_model.get_weights())
            model2.set_weights(model.get_weights())
        moves_base_power = -np.ones(4)
        moves_dmg_multiplier = np.ones(4)
        for i, move in enumerate(battle.available_moves):
            moves_base_power[i] = (
                move.base_power / 100
            )  # Simple rescaling to facilitate learning
            if move.type:
                moves_dmg_multiplier[i] = move.type.damage_multiplier(
                    battle.opponent_active_pokemon.type_1,
                    battle.opponent_active_pokemon.type_2,
                )

        # We count how many pokemons have not fainted in each team
        remaining_mon_team = (
            len([mon for mon in battle.team.values() if mon.fainted]) / 6
        )
        remaining_mon_opponent = (
            len([mon for mon in battle.opponent_team.values() if mon.fainted]) / 6
        )

        # Final vector with 10 components
        moves = np.concatenate(
            [
                moves_base_power,
                moves_dmg_multiplier,
                [remaining_mon_team, remaining_mon_opponent],
            ]
        )
        prediction = model2(np.reshape(np.array(moves, dtype=np.float32), [1, 1, 10])).numpy()
        prediction = np.reshape(prediction, [14])
        #print(prediction)
        action = prediction.tolist().index(max(prediction))
        if (
            action < 4
            and action < len(battle.available_moves)
            and not battle.force_switch
        ):
            return self.create_order(battle.available_moves[action])
        elif (
            battle.can_dynamax
            and 0 <= action - 4 < len(battle.available_moves)
            and not battle.force_switch
        ):
            return self.create_order(battle.available_moves[action - 4], dynamax=True)
        elif 0 <= action - 8 < len(battle.available_switches):
            return self.create_order(battle.available_switches[action - 8])
        else:
            return self.choose_random_move(battle)

class TrainedRLOpponent(Player):
      def choose_move(self, battle):
        moves_base_power = -np.ones(4)
        moves_dmg_multiplier = np.ones(4)
        for i, move in enumerate(battle.available_moves):
            moves_base_power[i] = (
                move.base_power / 100
            )  # Simple rescaling to facilitate learning
            if move.type:
                moves_dmg_multiplier[i] = move.type.damage_multiplier(
                    battle.opponent_active_pokemon.type_1,
                    battle.opponent_active_pokemon.type_2,
                )

        # We count how many pokemons have not fainted in each team
        remaining_mon_team = (
            len([mon for mon in battle.team.values() if mon.fainted]) / 6
        )
        remaining_mon_opponent = (
            len([mon for mon in battle.opponent_team.values() if mon.fainted]) / 6
        )

        # Final vector with 10 components
        moves = np.concatenate(
            [
                moves_base_power,
                moves_dmg_multiplier,
                [remaining_mon_team, remaining_mon_opponent],
            ]
        )
        prediction = old_model(np.reshape(np.array(moves, dtype=np.float32), [1, 1, 10])).numpy()
        prediction = np.reshape(prediction, [14])
        #print(prediction)
        action = prediction.tolist().index(max(prediction))
        if (
            action < 4
            and action < len(battle.available_moves)
            and not battle.force_switch
        ):
            return self.create_order(battle.available_moves[action])
        elif (
            battle.can_dynamax
            and 0 <= action - 4 < len(battle.available_moves)
            and not battle.force_switch
        ):
            return self.create_order(battle.available_moves[action - 4], dynamax=True)
        elif 0 <= action - 8 < len(battle.available_switches):
            return self.create_order(battle.available_switches[action - 8])
        else:
            return self.choose_random_move(battle)

class MaxDamagePlayer(RandomPlayer):
    def choose_move(self, battle):
        # If the player can attack, it will
        if battle.available_moves:
            # Finds the best move among available ones
            best_move = max(battle.available_moves, key=lambda move: move.base_power)
            return self.create_order(best_move)

        # If no attack is available, a random switch will be made
        else:
            return self.choose_random_move(battle)


NB_TRAINING_STEPS = 1000000
NB_EVALUATION_EPISODES = 1000

tf.random.set_seed(1234)
np.random.seed(1234)

def decision(probability):
    return random.random() < probability

# This is the function that will be used to train the dqn
def dqn_training(player, nb_steps):
    act = deepq.learn(
        env,
        network='conv_only',
        lr=0.00025,
        total_timesteps=10000,
        buffer_size=50000,
        exploration_fraction=0.1,
        exploration_final_eps=0.02,
        print_freq=10,
    )
    print("Saving model to cartpole_model.pkl")
    act.save("cartpole_model.pkl")
    player.complete_current_battle()

if __name__ == "__main__":
    env_player = SimpleRLPlayer(battle_format="gen8randombattle")
    opponent = RandomPlayer(battle_format="gen8randombattle")
    second_opponent = MaxDamagePlayer(battle_format="gen8randombattle")
    third_opponent = TrainedRLPlayer(battle_format="gen8randombattle")
    fourth_opponent = TrainedRLOpponent(battle_format="gen8randombattle")

    env_player.play_against(
        env_algorithm=dqn_training,
        opponent=third_opponent,
        env_algorithm_kwargs={"nb_steps": NB_TRAINING_STEPS},
    )
