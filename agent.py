# -*- coding: utf-8 -*-
import numpy as np
import tensorflow as tf
import random
import matplotlib.pyplot as plt

from poke_env.player.env_player import Gen8EnvSinglePlayer
from poke_env.player.random_player import RandomPlayer
from poke_env.player.player import Player

from rl.agents.dqn import DQNAgent
from rl.policy import LinearAnnealedPolicy, EpsGreedyQPolicy
from rl.memory import SequentialMemory
from tensorflow.keras.layers import Lambda, Dense, Flatten
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import load_model
import tensorflow.keras.backend as K
from tensorflow.keras.models import Model

model_num = 1
model = Sequential()
model2 = Sequential()
old_model = load_model("models/model_9")
dqn = None

# We define our RL player
# It needs a state embedder and a reward computer, hence these two methods
class SimpleRLPlayer(Gen8EnvSinglePlayer):
    def embed_battle(self, battle):
        # -1 indicates that the move does not have a base power
        # or is not available
        moves_base_power = -np.ones(8)
        moves_dmg_multiplier = np.ones(8)
        for i, move in enumerate(battle.available_moves):
            moves_base_power[i] = (
                move.base_power / 100
            )  # Simple rescaling to facilitate learning
            if move.type:
                moves_dmg_multiplier[i] = move.type.damage_multiplier(
                    battle.opponent_active_pokemon.type_1,
                    battle.opponent_active_pokemon.type_2,
                )
            if (battle.can_dynamax):
                moves_base_power[i+4] = (
                    move.dynamaxed.base_power / 100
                )
                if move.dynamaxed.type:
                    moves_dmg_multiplier[i+4] = move.dynamaxed.type.damage_multiplier(
                        battle.opponent_active_pokemon.type_1,
                        battle.opponent_active_pokemon.type_2,
                    )
            else:
                moves_base_power[i+4] = (
                    0
                )
                if move.dynamaxed.type:
                    moves_dmg_multiplier[i+4] = move.dynamaxed.type.damage_multiplier(
                        battle.opponent_active_pokemon.type_1,
                        battle.opponent_active_pokemon.type_2,
                    )
            if move.type == battle.active_pokemon.type1 or move.type == battle.active_pokemon.type2:
                moves_dmg_multiplier[i] = moves_dmg_multiplier[i] * 1.5
                moves_dmg_multiplier[i+4] = moves_dmg_multiplier[i]

        # We count how many pokemons have not fainted in each team
        remaining_mon_team = (
            len([mon for mon in battle.team.values() if mon.fainted]) / 6
        )
        remaining_mon_opponent = (
            len([mon for mon in battle.opponent_team.values() if mon.fainted]) / 6
        )

        # Final vector with 10 components
        return np.concatenate(
            [
                moves_base_power,
                moves_dmg_multiplier,
                [remaining_mon_team, remaining_mon_opponent],
            ]
        )

    def compute_reward(self, battle) -> float:
        reward = self.reward_computing_helper(
            battle, fainted_value=2, hp_value=1, victory_value=30
        )
        return reward

class TrainedRLPlayer(Player):
      def choose_move(self, battle):
        #if (decision(0.7)):
        #    #try:
        #    tmp = load_model(cfilepath)
        #    model2.set_weights(tmp.get_weights())
            #except:
            #    model2.load_weights(old_model.get_weights())
        if (decision(0.7)):
            model2.set_weights(dqn.model.get_weights())
        else:
            if (decision(0.5)):
                if battle.available_moves:
                    # Finds the best move among available ones
                    best_move = max(battle.available_moves, key=lambda move: move.base_power)
                    return self.create_order(best_move)

                # If no attack is available, a random switch will be made
                else:
                    return self.choose_random_move(battle)
        moves_base_power = -np.ones(8)
        moves_dmg_multiplier = np.ones(8)
        for i, move in enumerate(battle.available_moves):
            moves_base_power[i] = (
                move.base_power / 100
            )  # Simple rescaling to facilitate learning
            if move.type:
                moves_dmg_multiplier[i] = move.type.damage_multiplier(
                    battle.opponent_active_pokemon.type_1,
                    battle.opponent_active_pokemon.type_2,
                )
            moves_base_power[i+4] = (
                move.dynamaxed.base_power / 100
            )
            if move.dynamaxed.type:
                moves_dmg_multiplier[i+4] = move.dynamaxed.type.damage_multiplier(
                    battle.opponent_active_pokemon.type_1,
                    battle.opponent_active_pokemon.type_2,
                )

        # We count how many pokemons have not fainted in each team
        remaining_mon_team = (
            len([mon for mon in battle.team.values() if mon.fainted]) / 6
        )
        remaining_mon_opponent = (
            len([mon for mon in battle.opponent_team.values() if mon.fainted]) / 6
        )

        # Final vector with 10 components
        moves = np.concatenate(
            [
                moves_base_power,
                moves_dmg_multiplier,
                [remaining_mon_team, remaining_mon_opponent],
            ]
        )
        prediction = model2(np.reshape(np.array(moves, dtype=np.float32), [1, 1, 18])).numpy()
        prediction = np.reshape(prediction, [14])
        #print(prediction)
        action = prediction.tolist().index(max(prediction))
        if (
            action < 4
            and action < len(battle.available_moves)
            and not battle.force_switch
        ):
            return self.create_order(battle.available_moves[action])
        elif (
            battle.can_dynamax
            and 0 <= action - 4 < len(battle.available_moves)
            and not battle.force_switch
        ):
            return self.create_order(battle.available_moves[action - 4], dynamax=True)
        elif 0 <= action - 8 < len(battle.available_switches):
            return self.create_order(battle.available_switches[action - 8])
        else:
            return self.choose_random_move(battle)

class TrainedRLOpponent(Player):
      def choose_move(self, battle):
        moves_base_power = -np.ones(4)
        moves_dmg_multiplier = np.ones(4)
        for i, move in enumerate(battle.available_moves):
            moves_base_power[i] = (
                move.base_power / 100
            )  # Simple rescaling to facilitate learning
            if move.type:
                moves_dmg_multiplier[i] = move.type.damage_multiplier(
                    battle.opponent_active_pokemon.type_1,
                    battle.opponent_active_pokemon.type_2,
                )

        # We count how many pokemons have not fainted in each team
        remaining_mon_team = (
            len([mon for mon in battle.team.values() if mon.fainted]) / 6
        )
        remaining_mon_opponent = (
            len([mon for mon in battle.opponent_team.values() if mon.fainted]) / 6
        )

        # Final vector with 10 components
        moves = np.concatenate(
            [
                moves_base_power,
                moves_dmg_multiplier,
                [remaining_mon_team, remaining_mon_opponent],
            ]
        )
        prediction = old_model(np.reshape(np.array(moves, dtype=np.float32), [1, 1, 10])).numpy()
        prediction = np.reshape(prediction, [14])
        #print(prediction)
        action = prediction.tolist().index(max(prediction))
        if (
            action < 4
            and action < len(battle.available_moves)
            and not battle.force_switch
        ):
            return self.create_order(battle.available_moves[action])
        elif (
            battle.can_dynamax
            and 0 <= action - 4 < len(battle.available_moves)
            and not battle.force_switch
        ):
            return self.create_order(battle.available_moves[action - 4], dynamax=True)
        elif 0 <= action - 8 < len(battle.available_switches):
            return self.create_order(battle.available_switches[action - 8])
        else:
            return self.choose_random_move(battle)

class MaxDamagePlayer(RandomPlayer):
    def choose_move(self, battle):
        # If the player can attack, it will
        if battle.available_moves:
            # Finds the best move among available ones
            best_move = max(battle.available_moves, key=lambda move: move.base_power)
            return self.create_order(best_move)

        # If no attack is available, a random switch will be made
        else:
            return self.choose_random_move(battle)

NB_TRAINING_STEPS = 1000000
NB_EVALUATION_EPISODES = 100

tf.random.set_seed(1234)
np.random.seed(1234)

def decision(probability):
    return random.random() < probability

# This is the function that will be used to train the dqn
def dqn_training(player, dqn, nb_steps, scores):
    history = dqn.fit(player, nb_steps=nb_steps)
    scores[:] = history.history["episode_reward"]
    player.complete_current_battle()


def dqn_evaluation(player, dqn, nb_episodes):
    # Reset battle statistics
    player.reset_battles()
    dqn.test(player, nb_episodes=nb_episodes, visualize=False, verbose=False)

    print(
        "DQN Evaluation: %d victories out of %d episodes"
        % (player.n_won_battles, nb_episodes)
    )

if __name__ == "__main__":
    env_player = SimpleRLPlayer(battle_format="gen8randombattle")
    opponent = RandomPlayer(battle_format="gen8randombattle")
    second_opponent = MaxDamagePlayer(battle_format="gen8randombattle")
    third_opponent = TrainedRLPlayer(battle_format="gen8randombattle")
    fourth_opponent = TrainedRLOpponent(battle_format="gen8randombattle")

    # Output dimension
    n_action = len(env_player.action_space)

    model.add(Dense(128, activation="elu", input_shape=(1, 18)))

    # Our embedding have shape (1, 18), which affects our hidden layer
    # dimension and output dimension
    # Flattening resolve potential issues that would arise otherwise
    model.add(Flatten())
    model.add(Dense(64, activation="elu"))
    model.add(Dense(n_action, activation="linear"))

    model2.add(Dense(128, activation="elu", input_shape=(1, 18)))

    # Our embedding have shape (1, 10), which affects our hidden layer
    # dimension and output dimension
    # Flattening resolve potential issues that would arise otherwise
    model2.add(Flatten())
    model2.add(Dense(64, activation="elu"))
    model2.add(Dense(n_action, activation="linear"))

    layer = model2.layers[-2]
    nb_action = model2.output.shape[-1]
    # layer y has a shape (nb_action+1,)
    # y[:,0] represents V(s;theta)
    # y[:,1:] represents A(s,a;theta)
    y = Dense(nb_action + 1, activation='linear')(layer.output)
    # caculate the Q(s,a;theta)
    # dueling_type == 'avg'
    # Q(s,a;theta) = V(s;theta) + (A(s,a;theta)-Avg_a(A(s,a;theta)))
    # dueling_type == 'max'
    # Q(s,a;theta) = V(s;theta) + (A(s,a;theta)-max_a(A(s,a;theta)))
    # dueling_type == 'naive'
    # Q(s,a;theta) = V(s;theta) + A(s,a;theta)
    outputlayer = Lambda(lambda a: K.expand_dims(a[:, 0], -1) + a[:, 1:] - K.mean(a[:, 1:], axis=1, keepdims=True), output_shape=(nb_action,))(y)

    model2 = Model(inputs=model2.input, outputs=outputlayer)
    memory = SequentialMemory(limit=50000, window_length=1)

    # Ssimple epsilon greedy
    policy = LinearAnnealedPolicy(
        EpsGreedyQPolicy(),
        attr="eps",
        value_max=1.0,
        value_min=0.1,
        value_test=0.05,
        nb_steps=1000000,
    )

    # Defining our DQN
    dqn = DQNAgent(
        model=model,
        nb_actions=len(env_player.action_space),
        policy=policy,
        memory=memory,
        nb_steps_warmup=1000,
        gamma=0.5,
        target_model_update=1000,
        delta_clip=0.01,
        enable_double_dqn=True,
        enable_dueling_network=True,
        dueling_type='avg',
    )

    dqn.compile(Adam(lr=0.0025), metrics=["mae"])
    # Training
    scores = []
    env_player.play_against(
        env_algorithm=dqn_training,
        opponent=third_opponent,
        env_algorithm_kwargs={"dqn": dqn, "nb_steps": NB_TRAINING_STEPS, "scores": scores},
    )
    print(model.summary())
    dqn.model.save("new_models/model_%d" % model_num)
    new_scores = np.array(scores)
    new_scores = np.nanmean(np.pad(new_scores.astype(float), (0, 100 - new_scores.size%100), mode='constant', constant_values=np.NaN).reshape(-1, 100), axis=1)
    xcord = [i for i in range(len(new_scores))]
    plt.plot(xcord, new_scores, 'b', label="Scores")
    plt.title('Learning Curve', fontsize=16)
    plt.xlabel('Episodes', fontsize=16)
    plt.ylabel('Reward', fontsize=16)
    plt.savefig("fig_%d.png" % model_num)
    # plt.show()
    # print("Results against random player:")
    # env_player.play_against(
    #     env_algorithm=dqn_evaluation,
    #     opponent=opponent,
    #     env_algorithm_kwargs={"dqn": dqn, "nb_episodes": NB_EVALUATION_EPISODES},
    # )
    #
    # print("\nResults against max player:")
    # env_player.play_against(
    #     env_algorithm=dqn_evaluation,
    #     opponent=second_opponent,
    #     env_algorithm_kwargs={"dqn": dqn, "nb_episodes": NB_EVALUATION_EPISODES},
    # )
    #
    # print("\nResults against train player:")
    # env_player.play_against(
    #     env_algorithm=dqn_evaluation,
    #     opponent=fourth_opponent,
    #     env_algorithm_kwargs={"dqn": dqn, "nb_episodes": NB_EVALUATION_EPISODES},
    # )
